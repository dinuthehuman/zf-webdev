# CSS

## Concepts

A rule consists of a _selector_ and several _bindings_. Bindings are made up of a key and a value.

## Rule priority

- Specifity: Je weniger Elemente eine Regel selektiert, desto höher ist sie gewichtet
    - Dabei wird folgende Entscheidungshierarchie angewendet: Ids > Attributes > Tags
- Cascading: Wenn mehrere Regeln in Konflikt stehen, wird die letzte Regel angewendet
- Inheritence: Gewisse Bindings werden vererbt (etwa `font-family`), gewisse nicht (etwa `border`)

## Selectors

All descendants selector:

```CSS
header p { ... }     /* selects all p elements within the header */
```

Direct descendant selector:

```CSS
main > img { ... }   /* selects all img children of main */
```

Attribute based selectors:

```CSS
a[title] { ... }          /* selects all links with a title attribute */
a[href='http'] { ... }    /* selects all links with an href starting with 'http' */
```

## CSS Variables

In CSS können Variabeln definiert werden, deren Scope entweder innerhalb der Regel gilt, in der sie definiert werden, oder global, wenn sie in `:root{}` definiert werden.

Beispiel:

```CSS
:root {
    --header-color: #0000ff;
}

h1 {
    color: var(--header-color);
    font-size: 32px;
}
h2 {
    color: var(--header-color);
    font-size: 24px;
}
```