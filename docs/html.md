# HTML

## Essential headers

Doctype & Opening Tag

```html
<!DOCTYPE html>
<html lang="en">
</html>
```

## Head with essentials

```html
<head>
    <meta charset="utf-8">
    <title>Tutorial</title>
    <!-- Style sheet inclusion -->
    <link rel="stylesheet" href="bootstrap.min.css">
    <!-- Javascript inclusion -->
    <script src="jquery.min.js"></script>
    <!-- responsive design specific -->
    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
```

## Form Input Validation Examples

```html
<form action="/register.php" method="post">
    <label>Username:</label>
    <input name="username" pattern="[a-z0-9_]+" required><br>
    <label>Password:</label>
    <input name="password" minlength="5" required><br>
    <label>Email:</label>
    <input name="email" type="email" required><br>
    <input type="submit" value="Register">
</form>
```
