# JQuery

## Element Selection

Alle Tags, z.B. alle `<p>` Tags:

```javascript
$("p")
```

Auf Basis des Klassen-Namens:

```javascript
$(".someClass")
```

Auf Basis der ID:

```javascript
$("#myId")
```

Auf Basis eines Attributes, v.a. nützlich mit eigenen `data-*` Attributen:

```javascript
// Alle a mit target "_blank":
$("a[target='_blank']");

// Alle anderen a:
$("a[target!='_blank']")

// Eigenes Attribut data-myOwnAttribute
$("a[data-myOwnAttribute='something']")
```

Mit Arrows über die Ergebnisse iterieren:

```javascript
$("p").each((index, element) => {
    console.log("hiding p tag with index nr. " + index);
    element.hide();
});
```

## AJAX

Fundamentales Beispiel mit _Promises_:

```javascript
// Angenommen, es gibt irgendein Content Objekt, das ich verschicken will
let content = new ContentObj(c);
// Angenommen, es gibt ein User-Objekt, dass Username und Passwort enthält
let user = new User(dinu, password123);
// Return type ist ein Promise, "let" nicht vergessen!
let myPromise = $.ajax({
    // URL ist ziemlich offensichtlich
    url: "/api/users",
    // TYPE, nicht method! Kann "post", "get", "put", "delete"... sein
    type: "post",
    // Benutzen, wenn ich *VOM* Server JSON erwarte; 
    // VORSICHT: produziert einen Fehler, wenn ich vom Server keine Antwort erhalte
    dataType: "json",
    // Benutzen, wenn ich *AN* den Server JSON schicken will
    contentType: "application/json; charset=utf-8",
    // Wenn ich ein instanziertes Objekt als JSON verschicken will, muss ich es erst konvertieren
    data: JSON.stringify(content),
    // Angenommen, ich muss Zugangsdaten mitschicken
    headers: {
        "Authorization": "Basic " + btoa(user.name + ":" + user.password)
    }
});
```