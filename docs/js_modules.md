# Javascript Modules

## Browser-Import as Module

```html
<script type="module" src="my_script.js"></script>
```

## Export Syntax

export on definition:

```javascript
export var x = 12;
export function doSomething(param) {
    return param;
}
```

export explicitly:

```javascript
var x = 12;
function doSomething(param) {
    return param;
}

export {doSomething, x}
```

## Import Syntax

_If not working with_ `npm` _remember to add **.js** to all paths!!_

```javascript
import { doSomething, x } from './other_module.js';
```

Rename on import:

```javascript
import { doSomething as doSomethingElse, x as y } from './other_module.js';
```
