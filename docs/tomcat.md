# Tomcat

## Installation

1. Download the ZIP archive of the Tomcat 10 server from [https://tomcat.apache.org/download-10.cgi](https://tomcat.apache.org/download-10.cgi)
2. Extract the archive to a sensible directory, e.g. `C:\apache-tomcat-10.0.10`

## Run from IntelliJ

1. In IntelliJ, open your Maven web project, e.g. `servlets`
2. In the Run menu, choose _Edit Configurations..._
3. In the popup window
    - Click the plus sign to add a new configuration and choose _Tomcat Server Local_ from the list
    - Enter `Tomcat` as configuration name
    - Click the _Configure_ button of the application server and enter Tomcat's installation directory
    - In the _Deployment_ tab, click the plus sign and add `servlets:war exploded` as artifact
    - Enter `/` as application context
    - In the _Server_ tab, choose `Update classes and resources` on frame deactivation

_Server Config:_

![Server Config](img/server.jpg)

_Deployment Config:_

![Deployment Config](img/deployment.jpg)