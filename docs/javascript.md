# Javascript

## Loading/Including

### Hosted jQuery

```html
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
```

### Load JS deferred

```html
<script src="demo_defer.js" defer></script> 
```

## Template Literals

Templating direkt in Strings, ähnl. Pythons f"" Syntax:

```javascript
const student = {
name: "Bob",
age: 25,
university: "BFH"
};
const message = `${student.name} is a student at ${student.university}.
${student.name} was born in ${getYear() - student.age}.`;
```

## JQuery Element Selection



## Arrow functions

Lambda-like Syntax, wobei die Parameter in Klammer (oder ohne für nur einen) geschrieben werden und der Function-Body danach

```javascript
let myFunction = (a, b) => a * b;

console.log(myFunction(2, 3)); // Logs "6"
```

- `return` ist unnötig, wenn eindeutig

_Caution_: Unterschiede zu traditionellen Funktionen:

- Does not have its own bindings to this or super, and should not be used as methods.
- Does not have new.target keyword.
- Not suitable for call, apply and bind methods, which generally rely on establishing a scope.
- Can not be used as constructors.
- Can not use yield, within its body.

## Promises

### Grundlegende Syntax

```javascript
function computeAsync() {
    return new Promise((resolve, reject) => {
        // ... Perform the asynchronous task (Promise is pending)
        if (success) resolve(result); // Promise will be fulfilled
        else reject(error); // Promise will be rejected
    });
}
```

### Beispiel

```javascript
function computePI() {
    return new Promise((resolve, reject) => {
        // Computing PI is hard work...
        setTimeout(() => {
            // Computing PI fails with a certain probability...
            if (Math.random() < 0.2) {
                reject("Sorry, computation failed!"); // Reject the promise
            } 
            else {
                resolve(3.14); // Resolve the promise to the value of PI
            }
        }, 300);
    });
}

// Callbacks & Catchers
computePI().then(result => console.log("PI: " + result)).catch(error => console.log(error));
```

### Callbacks & Combinations

Chainging callbacks, vorausgesetzt `doAsync()` gibt ein Promise zurück

```javascript
doAsync()
    .then(resultA => { /* ... */ })
    .then(resultB => { /* ... */ })
    .catch(error => { /* ... */ })
    .then(resultC => { /* ... */ })
    .catch(error => { /* ... */ });
```

Mehrere Callbacks ans gleiche Promise hängen:

```javascript
const promise = doAsync();
promise.then(result => { /* ... */ });
promise.then(result => { /* ... */ });
```

Mehrere Promises, auf **alle** warten (oder erster Reject):

```javascript
Promise.all([doAsync("A"), doAsync("B"), doAsync2()])
    .then(results => { /* ... */ })
    .catch(error => { /* ... */ });
```

Mehrere Promises, auf das **erste** warten (oder erster Reject):

```javascript
Promise.race([doAsync("A"), doAsync("B"), doAsync2()])
    .then(result => { /* ... */ })
    .catch(error => { /* ... */ });
```
