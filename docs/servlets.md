# Java Servlet Crap

## Request Methods

Nach verwendeter Methode fragen, z.B. DELETE:

```java
request.getMethod().equals("DELETE")
```

Path analysieren, geht von folgenden Bedingungen aus:

- Annotation lautet `@WebServlet("/api/messages/*")`
- Ich analysiere so nur den Teil ab `/*`, inclusive Slash
- Das Beispiel parst zu int, valide ist z.B. `/api/messages/8`

```java
public void doDelete(HttpServletRequest request, HttpServletResponse response) throws IOException {
    String extraPath = request.getPathInfo();
    if(extraPath == null) {
        response.setStatus(HttpServletResponse.SC_BAD_REQUEST);
        return;
    }
    String[] parts = extraPath.split("/");
    int id = Integer.parseInt(parts[1]);
    chatService.removeMessage(id);
    response.setStatus(HttpServletResponse.SC_NO_CONTENT);
}
```

## Deployment Descriptor

Weil fucking Java, gibt's natürlich ein `web.xml`, der Deployment Descriptor, der Dinge festlegt wie:

- Entry Point/Directory Index
- Session Timeout

_Example:_

```xml
<?xml version="1.0" encoding="UTF-8"?>
<web-app version="5.0" xmlns="https://jakarta.ee/xml/ns/jakartaee"
    xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="https://jakarta.ee/xml/ns/jakartaee
                        https://jakarta.ee/xml/ns/jakartaee/web-app_5_0.xsd">
    <welcome-file-list>
        <welcome-file>index.html</welcome-file>
    </welcome-file-list>
    <session-config>
        <session-timeout>10</session-timeout>
    </session-config>
</web-app>
```

## Basic Get

```java
@WebServlet("/time")
public class TimeServlet extends HttpServlet {

    public void doGet(HttpServletRequest request, HttpServletResponse response)
            throws IOException {
        response.setContentType("text/plain");
        try (PrintWriter out = response.getWriter()) {
            out.println(LocalDateTime.now().withNano(0));
        }
    }
}
```

## Basic Post

```java
@WebServlet("/chat")
public class ChatServlet extends HttpServlet {

    private static final List<String> messages = new ArrayList<>();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException { 
        ...
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        String message = request.getParameter("message");
        messages.add(message);
        doGet(request, response);
    }
}
```

## Methods of indirection

```java
// include the response of another servlet
request.getRequestDispatcher("other").include(request, response)
// forward a request to another servlet
request.getRequestDispatcher("other").forward(request, response)
// redirect the client to another servlet
response.sendRedirect(request.getContextPath() + "other")
```

## Attach data to redirection

Use _attributes_!

```java
request.setAttribute("greeting", greeting);
String greeting = request.getAttribute("greeting");
```

## Redirect to JSP (templating)

```java
request.setAttribute("messages", messages);
request.getRequestDispatcher("chat.jsp").forward(request, response);
```

_Basic template:_

```html
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <meta charset="utf-8">
    <title>Chat JSP</title>
</head>
<body>
    <h1>Chat</h1>
    <form action="chat" method="post">
        <label>Your Message:</label>
        <input type="text" name="message" size="40" required>
        <input type="submit" value="Post">
    </form>
    <!-- FOR LOOP -->
    <c:forEach items="${messages}" var="message">
        <p>${message.date}<br>${message.text}</p>
    </c:forEach>

    <!-- SIMPLE IF -->
    <c:if test="${messages.size()=='0'}">
        <p>No messsages</p>
    </c:if>

    <!-- OVERCOMPLEX IF/ELSE -->
    <c:choose>
        <c:when test="${messages.size()=='0'}">
            <p>No messsages</p>
        </c:when>    
        <c:otherwise>
            <p>Some messsages</p>
        </c:otherwise>
    </c:choose>
</body>
</html>
```

## Save to Session

```java
HttpSession session = request.getSession();
session.setAttribute("counter", counter + 1);
Integer counter = (Integer) session.getAttribute("counter");
```

## Basic Filter Example

```java
@WebFilter("/*")
public class LoggingFilter extends HttpFilter {

    private static final Logger logger = Logger.getLogger(LoggingFilter.class.getName());

    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        logger.info("Request URI: " + request.getRequestURI());
        chain.doFilter(request, response);
        logger.info("Response status: " + response.getStatus());
    }
}
```

## Servlet Basic Auth/Servlet Filter Example

Zugangsdaten in Base64 mitsenden:

```javascript
$.ajax({
    ...
    headers: { "Authorization": "Basic " + btoa(username + ":" + password) }
});
```

...und serverseitig überprüfen:

```java
@WebFilter("/orders/*")
public class AuthenticationFilter extends HttpFilter {

    public void doFilter(HttpServletRequest request, HttpServletResponse response, FilterChain chain) throws IOException, ServletException {
        try {
            // ZUGANGSDATEN EXTRAHIEREN
            String[] credentials = getCredentials(request);
            // ...und überprüfen
            validateCredentials(credentials);
        // "return" bricht die Bearbeitung des Requests sofort ab
        } catch (Exception ex) { response.setStatus(401); return; }
        chain.doFilter(request, response);
    }

    // getHeader nutzen, um isolierte Zugangsdaten zu erhalten
    private String[] getCredentials(HttpServletRequest request) throws Exception {
        String authHeader = request.getHeader("Authorization");
        String[] tokens = authHeader.split(" ");
        if (!tokens[0].equals("Basic")) throw new Exception();
        byte[] decoded = Base64.getDecoder().decode(tokens[1]);
        return new String(decoded, StandardCharsets.UTF_8).split(":");
    }

    // Irgendwas mit DB oder was auch immer machen
    private void validateCredentials(String[] credentials) throws Exception {
        // throw exception if not valid
    }
}
```
