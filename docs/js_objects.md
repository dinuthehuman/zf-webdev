# Javascript Objects

## Prototype Syntax Example

```javascript
// Constructor with prototype function
function Person(name) {
    this.name = name;
}
Person.prototype.say = function(text) {
    console.log(this.name + " says " + text);
};
```

## Prototype Chaining

_Vererbt die Methoden von_ `Person` _an_ `Student`

```javascript
// Extending the constructor
function Student(name, subject) {
    Person.call(this, name);
    this.subject = subject;
}
// Prototype chaining
Student.prototype = Object.create(Person.prototype);
Student.prototype.constructor = Student;

let alice = new Student("Alice", "biology");
alice.say("Hello");                      //-> Alice says Hello
console.log(alice instanceof Person);    //-> true
```

## Class Syntax

_syntaktischer Zucker; ist mit Prototypen-Syntax identisch_

```javascript
class Person {
    // The - well - constructor; don't forget NEW
    constructor(name) {
        this.name = name;
    }

    // Simple method
    say(text) {
        console.log(this.name + " says " + text);
    }
}

// It's not Python. Don't forget NEW! Don't forget LET!
let alice = new Person("Alice");
alice.say("hi bob");
```
