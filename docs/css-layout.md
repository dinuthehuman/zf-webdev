# CSS Layout

## Float example

_CSS:_

```css
* { box-sizing: border-box; }
main { display: flow-root; }
section {
    float: left;
    width: 70%;
}
aside {
    float: right;
    width: 30%;
}
```

_HTML:_

```html
<body>
    <header>Header</header>
    <main>
        <section>Section</section>
        <aside>Aside</aside>
    </main>
    <footer>Footer</footer>
</body>
```

## Flex Example

_CSS:_

```css
body {
    display: flex;
    flex-direction: column;
}
main {
    display: flex;
    flex-direction: row;
}
section { flex: 7; }    /* sizing factor */
aside   { flex: 3; }
```

_HTML:_

```html
<body>
    <header>Header</header>
    <main>
        <section>Section</section>
        <aside>Aside</aside>
    </main>
    <footer>Footer</footer>
</body>
```

## Grid Example

_CSS:_

```css
body {
    display: grid;
    grid-template-rows: auto auto auto;
    grid-template-columns: 7fr 3fr;      /* width fractions */
}
header  { grid-area: 1 / 1 / span 1 / span 2; }
section { grid-area: 2 / 1; }
aside   { grid-area: 2 / 2; }
footer  { grid-area: 3 / 1 / span 1 / span 2; }
```

_HTML:_

```html
<body>
    <header>Header</header>
    <section>Section</section>
    <aside>Aside</aside>
    <footer>Footer</footer>
</body>
```
